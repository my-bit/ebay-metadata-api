# # HazardousMaterialDetailsResponse

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**signal_words** | [**\MyBit\Ebay\Metadata\Model\SignalWord[]**](SignalWord.md) | This array contains available hazardous materials signal words for the specified marketplace. | [optional]
**statements** | [**\MyBit\Ebay\Metadata\Model\HazardStatement[]**](HazardStatement.md) | This array contains available hazardous materials hazard statements for the specified marketplace. | [optional]
**pictograms** | [**\MyBit\Ebay\Metadata\Model\Pictogram[]**](Pictogram.md) | This array contains available hazardous materials hazard pictograms for the specified marketplace. | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
