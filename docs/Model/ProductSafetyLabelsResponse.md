# # ProductSafetyLabelsResponse

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**pictograms** | [**\MyBit\Ebay\Metadata\Model\ProductSafetyLabelPictogram[]**](ProductSafetyLabelPictogram.md) | This array contains a list of pictograms of product safety labels  for the specified marketplace. | [optional]
**statements** | [**\MyBit\Ebay\Metadata\Model\ProductSafetyLabelStatement[]**](ProductSafetyLabelStatement.md) | This array contains available product safety labels statements for the specified marketplace. | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
