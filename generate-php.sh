#!/bin/bash

openapi-generator generate -i sell_metadata_v1_oas3.yaml -g php -o . --additional-properties=apiPackage=Api,invokerPackage=MyBit\\Ebay\\Metadata,artifactVersion=1.1.0,composerPackageName=my-bit/ebay-metadata-api