<?php
/**
 * HazardousMaterialDetailsResponseTest
 *
 * PHP version 7.4
 *
 * @category Class
 * @package  MyBit\Ebay\Metadata
 * @author   OpenAPI Generator team
 * @link     https://openapi-generator.tech
 */

/**
 * Metadata API
 *
 * The Metadata API has operations that retrieve configuration details pertaining to the different eBay marketplaces. In addition to marketplace information, the API also has operations that get information that helps sellers list items on eBay.
 *
 * The version of the OpenAPI document: v1.7.1
 * Generated by: https://openapi-generator.tech
 * Generator version: 7.6.0
 */

/**
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Please update the test case below to test the model.
 */

namespace MyBit\Ebay\Metadata\Test\Model;

use PHPUnit\Framework\TestCase;

/**
 * HazardousMaterialDetailsResponseTest Class Doc Comment
 *
 * @category    Class
 * @description A type that defines the response fields for the &lt;b&gt;getHazardousMaterialsLabels&lt;/b&gt; method.
 * @package     MyBit\Ebay\Metadata
 * @author      OpenAPI Generator team
 * @link        https://openapi-generator.tech
 */
class HazardousMaterialDetailsResponseTest extends TestCase
{

    /**
     * Setup before running any test case
     */
    public static function setUpBeforeClass(): void
    {
    }

    /**
     * Setup before running each test case
     */
    public function setUp(): void
    {
    }

    /**
     * Clean up after running each test case
     */
    public function tearDown(): void
    {
    }

    /**
     * Clean up after running all test cases
     */
    public static function tearDownAfterClass(): void
    {
    }

    /**
     * Test "HazardousMaterialDetailsResponse"
     */
    public function testHazardousMaterialDetailsResponse()
    {
        // TODO: implement
        self::markTestIncomplete('Not implemented');
    }

    /**
     * Test attribute "signal_words"
     */
    public function testPropertySignalWords()
    {
        // TODO: implement
        self::markTestIncomplete('Not implemented');
    }

    /**
     * Test attribute "statements"
     */
    public function testPropertyStatements()
    {
        // TODO: implement
        self::markTestIncomplete('Not implemented');
    }

    /**
     * Test attribute "pictograms"
     */
    public function testPropertyPictograms()
    {
        // TODO: implement
        self::markTestIncomplete('Not implemented');
    }
}
