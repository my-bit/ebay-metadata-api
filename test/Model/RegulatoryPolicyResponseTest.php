<?php
/**
 * RegulatoryPolicyResponseTest
 *
 * PHP version 7.4
 *
 * @category Class
 * @package  MyBit\Ebay\Metadata
 * @author   OpenAPI Generator team
 * @link     https://openapi-generator.tech
 */

/**
 * Metadata API
 *
 * The Metadata API has operations that retrieve configuration details pertaining to the different eBay marketplaces. In addition to marketplace information, the API also has operations that get information that helps sellers list items on eBay.
 *
 * The version of the OpenAPI document: v1.8.0
 * Generated by: https://openapi-generator.tech
 * Generator version: 7.6.0
 */

/**
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Please update the test case below to test the model.
 */

namespace MyBit\Ebay\Metadata\Test\Model;

use PHPUnit\Framework\TestCase;

/**
 * RegulatoryPolicyResponseTest Class Doc Comment
 *
 * @category    Class
 * @description A type that defines the response fields for the &lt;b&gt;getRegulatoryPolicies&lt;/b&gt; method.
 * @package     MyBit\Ebay\Metadata
 * @author      OpenAPI Generator team
 * @link        https://openapi-generator.tech
 */
class RegulatoryPolicyResponseTest extends TestCase
{

    /**
     * Setup before running any test case
     */
    public static function setUpBeforeClass(): void
    {
    }

    /**
     * Setup before running each test case
     */
    public function setUp(): void
    {
    }

    /**
     * Clean up after running each test case
     */
    public function tearDown(): void
    {
    }

    /**
     * Clean up after running all test cases
     */
    public static function tearDownAfterClass(): void
    {
    }

    /**
     * Test "RegulatoryPolicyResponse"
     */
    public function testRegulatoryPolicyResponse()
    {
        // TODO: implement
        self::markTestIncomplete('Not implemented');
    }

    /**
     * Test attribute "regulatory_policies"
     */
    public function testPropertyRegulatoryPolicies()
    {
        // TODO: implement
        self::markTestIncomplete('Not implemented');
    }

    /**
     * Test attribute "warnings"
     */
    public function testPropertyWarnings()
    {
        // TODO: implement
        self::markTestIncomplete('Not implemented');
    }
}
